package com.rohitgopalan.pig;

public final class PigConstants {
	
	public static final int PLAYER_ONE = 1;
	public static final int PLAYER_TWO = 2;
	
	public static final int MAXIMUM_SCORE = 100;
}
