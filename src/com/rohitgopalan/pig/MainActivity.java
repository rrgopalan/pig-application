package com.rohitgopalan.pig;

import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class MainActivity extends Activity {
	TextView lblPlayerOne,lblPlayerTwo,lblPlayerOneScore,lblPlayerTwoScore;
	Button btnHold,btnRoll;
	int currentPlayer,player1Score,player2Score;
	ImageView imageView;
	Random randomDiceRoller;
	int[] diceValues = {1,2,3,4,5,6};
	
	/**
	 * Use dice value from 1-6 to get the corresponding image resource
	 * @param diceValue value on dice from 1 to 6
	 * @return image resource according to the dice value
	 */
	private int diceValueToDrawable(int diceValue)
	{
		switch(diceValue)
		{
			case 1:
				return R.drawable.die1;
			case 2:
				return R.drawable.die2;
			case 3:
				return R.drawable.die3;
			case 4:
				return R.drawable.die4;
			case 5:
				return R.drawable.die5;
			case 6:
				return R.drawable.die6;
		}
		return R.drawable.die1;
	}
	
	/**
	 * Update labels with regards to the player currently rolling the dice. 
	 */
	private void setLabels()
	{
		String p1Helper = (currentPlayer == PigConstants.PLAYER_ONE) ? " (rolling dice)" : "";
		String p2Helper = (currentPlayer == PigConstants.PLAYER_TWO) ? " (rolling dice)" : "";
		lblPlayerOne.setText("Player 1" + p1Helper + " : " + Integer.toString(player1Score));
		lblPlayerTwo.setText("Player 2" + p2Helper + " : " + Integer.toString(player2Score));
		
		if(currentPlayer == PigConstants.PLAYER_ONE)
		{
			lblPlayerOne.setTypeface(null, Typeface.BOLD);
			lblPlayerTwo.setTypeface(null, Typeface.NORMAL);
		}
		else
		{
			lblPlayerTwo.setTypeface(null, Typeface.BOLD);
			lblPlayerOne.setTypeface(null, Typeface.NORMAL);
		}
	}
	
	private void addToTotal(int value)
	{
		if(currentPlayer == PigConstants.PLAYER_ONE)
		{
			player1Score += value;
		}
		else
		{
			player2Score += value;
		}
	}
	
	/**
	 * Game is won when Player 1 or 2 reaches maximum score of 100
	 * @return true if player 1 or 2 reach maximum score otherwise false
	 */
	private boolean hasGameBeenWon()
	{
		return player1Score >= PigConstants.MAXIMUM_SCORE || player2Score >= PigConstants.MAXIMUM_SCORE;
	}
	
	/**
	 * Show alert message when game is over but offer to start a new game. 
	 */
	private void gameOver()
	{
		btnHold.setVisibility(View.INVISIBLE);
		btnRoll.setVisibility(View.INVISIBLE);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.game_over_title);
		int winner = (player1Score >= PigConstants.MAXIMUM_SCORE) ? PigConstants.PLAYER_ONE : PigConstants.PLAYER_TWO;
		builder.setMessage("That's the end of the game! Player " + Integer.toString(winner) + " has won. Would you like to start a new game?");
		builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				startNewGame();
			}
		});
		builder.setNegativeButton(android.R.string.no, null);
		builder.create().show();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		lblPlayerOne = (TextView)findViewById(R.id.lblPlayerOne);
		lblPlayerTwo = (TextView)findViewById(R.id.lblPlayerTwo);
		btnHold = (Button)findViewById(R.id.btnHold);
		btnRoll = (Button)findViewById(R.id.btnRoll);
		imageView = (ImageView)findViewById(R.id.diceImageView);
		randomDiceRoller = new Random();
		
		lblPlayerOne.setTextColor(Color.WHITE);
		lblPlayerTwo.setTextColor(Color.WHITE);
		
		startNewGame();
		
		//Switch current player once hold is pressed
		btnHold.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				currentPlayer = (currentPlayer == PigConstants.PLAYER_ONE) ? PigConstants.PLAYER_TWO : PigConstants.PLAYER_ONE;
				setLabels();
				btnHold.setVisibility(View.INVISIBLE);
			}
		});
		
		/**
		 * If dice roll is 1, switch current player
		 * otherwise add value to current player's total and make the hold button visible
		 * If a result has been reached, end the game
		 */
		btnRoll.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int diceValue = diceValues[randomDiceRoller.nextInt(6)];
				int drawable = diceValueToDrawable(diceValue);
				imageView.setImageResource(drawable);
				
				if(diceValue == 1)
				{
					currentPlayer = (currentPlayer == PigConstants.PLAYER_ONE) ? PigConstants.PLAYER_TWO : PigConstants.PLAYER_ONE;
					setLabels();
					btnHold.setVisibility(View.INVISIBLE);
				}
				else
				{
					btnHold.setVisibility(View.VISIBLE);
					addToTotal(diceValue);
					setLabels();
					
					if(hasGameBeenWon())
						gameOver();
				}
				
			}
		});
		
	}

	/**
	 * Start a new game by clearing the scores of both players and indicate that player 1 starts first.
	 */
	private void startNewGame() {
		player1Score = 0;
		player2Score = 0;
		currentPlayer = PigConstants.PLAYER_ONE;
		setLabels();
		btnHold.setVisibility(View.INVISIBLE);
		btnRoll.setVisibility(View.VISIBLE);
		imageView.setImageResource(diceValueToDrawable(1));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if(itemId == R.id.action_new_game)
		{
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.new_game_label);
			builder.setMessage(R.string.new_game_question);
			builder.setNegativeButton(android.R.string.no, null);
			builder.setPositiveButton(android.R.string.yes,new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					startNewGame();
				}
			});
			builder.create().show();
		}
		return super.onOptionsItemSelected(item);
	}
	
}
